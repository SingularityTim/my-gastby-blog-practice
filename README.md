<!-- AUTO-GENERATED-CONTENT:START (STARTER) -->
<p align="center">
  <a href="https://www.gatsbyjs.org">
    <img alt="Gatsby" src="https://www.gatsbyjs.org/monogram.svg" width="60" />
  </a>
</p>
<h1 align="center">
  How to use RPi to host HTTPS Gatsby's blog
</h1>

 This project includes the guidance for setup Linux environment (by using Raspberry Pi and Ubuntu-Linux) to host the HTTPS static webpage. Generate the Gatsby site by blog boilerplate, the starter ships with the main Gatsby configuration files you might need to get up and running blazing fast with the blazing-fast app generator for React.

 If you just want to host the static website, that only need web server like [Caddy](https://caddyserver.com/) and the pre-build folder. 

_Want to know more about my side projects? You can visit my blog to take a look! [Tim's TRYnERR Journey](https://www.diallelus.idv.tw)._

# 🛫 Prepare the Environment for Raspberry Pi 3B 
### Install Raspberry Pi OS

  1. Download **Ubuntu 19.10.1** (Eoan Ermine) from Ubuntu official websie.
  _You should always download the OS file from the trusted resource_[Ubuntu 19.10.1 Download page](http://cdimage.ubuntu.com/ubuntu-server/eoan/daily-preinstalled/current/HEADER.html)
  
  This is just a recommandation. You can choose any other Linux OS for SoC, anything support ARM64 will work with RPi 2/3/4. Ubuntu 19.10.1 is based on Linux **5.3** and a specific release for IoT SoC device such as Raspberry Pi. _If you want to know more about Ubuntu 19.10.1, you can go to [Ubunty WiKi Eoan Ermine - Release Notes](https://wiki.ubuntu.com/EoanErmine/ReleaseNotes)_

  2. Copy the image to the SD card.

  ***Find an Unix comupter can make your life easier, plug in your SD card for RPi then open the terminal***
  *Here I'm using MAC OS X for demo*

  ```bash
  $ diskutil list
  > /dev/disk2 (external, physical): ...
  > ...
  ```

  Find the SD card and unmount it, for example *`/dev/disk2`*

  ```bash
  $ diskutil unmountDisk /dev/disk2
  > Unmount of all volumes on disk2 was successful.
  ```

  After unmount SD, use **`dd`** to mirror the image to SD, for example if OS image saved at:
  **`$HOME/Storage/ubuntu-19.10.1-preinstalled-server-arm64+raspi3.img.xz`**

  ```bash
  $ xzcat $HOME/Storage/ubuntu-19.10.1-preinstalled-server-arm64+raspi3.img.xz |\
  sudo dd bs=32m of=/dev/rdisk2
  ```

  Once **`dd`** complete, it will re-mount the SD card, then you can see it named *`/boot`*.

## Setup WiFi and initiate SSH without start

### WiFi

  Create a file in *`/boot`* directory called *`wpa_supplicant.conf`*. The file should contain the following details. With this file in place, **Ubuntu**(**Debain**) will move it to *`/etc/wpa_supplicant/`* when the system is booted.
  
  ```bash
  $ cd /Volumes/boot && nano wpa_supplicant.conf
  >>> wpa_supplicant.conf
  # (for Raspbian Stretch needs to add ctrl_interface)
  ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
  network={
      ssid="YOUR_WIFI_ESSID"
      psk="************"
      key_mgmt=WPA-PSK
  }
  ```
  
  ```bash
  ctrl + x; Y; enter to save
  ```

### Enable SSH on Ubuntu(Debian) without a screen

  All you have to do is create an empty file called ssh. Then it's initated when booted.

  ```bash
  # Please make sure where your /boot is mounted, again this is demo by Mac OS X
  $ cd /Volumes/boot && touch ssh
  ```

### Setup for your special LCD screen

  If you use an external HDMI screen. To make it work properly, you probably need to pre-config it.
  For example, I'm using a 7" 1024x600 LCD screen, which is much easier for portable use cases.
  You need to add the resolution in the config file to make sure RPi knows how to display properly.
  ***For*** `hdmi_cvt`***, Use whatever your screen resolution is.***

  ```bash
  >>> boot/config.txt
  max_usb_current=1
  hdmi_group=2
  hdmi_mode=87
  hdmi_cvt 1024 600 60 6 0 0 0
  hdmi_drive=1
  ```
  
  ```bash
  ctrl + x; Y; enter to save
  ```

***Now, you are ready to plug in the SD card to your Pi and boot up to start!!***

## SSH into defult Ubuntu Pi
  
  1. Use **`ssh`** to connect your device, by defult.
    For `Ubuntu`, the defult values are:

  ```bash
  username: ubuntu
  password: ubuntu
  ```

  ```bash
  $ ssh ubuntu@ubuntu
  > Welcome to Ubuntu 19.10 (GNU/Linux 5.3.0-1014-raspi2 aarch64)
  > ...
  > Last login: Thu Dec 26 13:32:37 2019 from 192.168.*.*
  ```

  2. To change or switch user to what ever you like. For example, mine is ***singu***

  ```bash
  $ sudo adduser singu
  > then type-in your password
  > you can input all your information or keep those blank just simply hit Enter
  ```

  3. Give new user sudo privilege.
  Add the new create user into the `sudoers` list
  
  ```bash
  $ sudo nano /etc/sudoers
  >>> sudoers
  # User privilege specification
  root    ALL=(ALL:ALL) ALL
  singu ALL=(ALL:ALL) ALL
  ```

  ```bash
  ctrl + x; Y; enter to save
  ```

  4. Re-login by newuser
  ***If you wish, you can remove old user*** 
  **`--remove-home`** will clean all the file under it's *`$HOME`*

  ```bash
  $ sudo deluser --remove-home ubuntu
  > Looking for files to backup/remove ...
  > Removing files ...
  > Removing user `ubuntu' ...
  > Warning: group `ubuntu' has no more members.
  > Done.
  ```

  5. Change hostname
  You can change the hostname to your own desire, for example, mine is `rpiserver`

  ```bash
  $ sudo hostnamectl set-hostname rpiserver
  ```
  
  Your hostname now should be changed, you can re-login to confirm if it changed properly.

## ⚓️ Installation
  It's now the most exciting part. However, it's also the most frustrating part and needs lots of tuning or bring some searching skills to find out the problem from the internet. The guidance is based on my own step and many many try and error. If you found the steps are difficult to directly copy with your project, extract the idea and find some other article that can help you. There are many communities and participants that are very willing to bring helps to you. And trust me, most of the problems have been solved by others already.

### Up to Date

Make sure the source list is up-to-date and package is upgrade without conflict. This might take a while.

  ```bash
  $ sudo apt -y update
  > x packages been updated. x packages can be upgraded.
  $ sudo apt -y upgrade
  > ...
  ```

### Install related packages for NodeJS and Prerequisites packages

  ```bash
  $ sudo apt install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev git openssl libpng-dev sqlite3 lsof net-tools
  > x package been installed.

  $ sudo apt -y upgrade && sudo apt -y dist-upgrade
  > x package 
  ```

  ***You have below 2 options to install NodeJS to your RPi. I will recommand using nvm which is easier, but if you prefer, can still install from source.***

  A. **Install NodeJS by nvm**

  ```bash
  $ curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.1/install.sh | bash
  $ nvm install 10.14.1
  ```

  B. **Install latest NodeJS by source**

  ```bash
  $ sudo apt -y install dirmngr apt-transport-https lsb-release ca-certificates
  $ curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
  $ sudo apt -y install nodejs
  $ sudo apt -y  install gcc g++ make
  ```

### Install GatsbyJS

Install the GatsbyJS command line tools, -g means node module is for globally use.

  ```bash
  $ npm install -g gatsby-cli
  ```

***Now, you are ready to start the Gatsby project.***
***After your Gatsby project up and running, go to below parts for HTTPS setup and baemon service***

# 🚀 GatsbyJS Quick start
### Before Gatsby starter, you need to complete the installation of Gatsby-cli.
1.  **Create a Gatsby site.**

    Use the Gatsby CLI to create a new site, specifying the blog starter.

    ```bash
    # create a new Gatsby site using the blog starter
    gatsby new my-blog-starter https://github.com/gatsbyjs/gatsby-starter-blog
    ```

1.  **Start developing.**

    Navigate into your new site’s directory and start it up.

    ```bash
    cd my-blog-starter/
    gatsby develop
    ```

1.  **Open the source code and start editing!**

    Your site is now running at `http://localhost:8000`!

    _Note: You'll also see a second link: _`http://localhost:8000/___graphql`_. This is a tool you can use to experiment with querying your data. Learn more about using this tool in the [Gatsby tutorial](https://www.gatsbyjs.org/tutorial/part-five/#introducing-graphiql)._

    Open the `my-blog-starter` directory in your code editor of choice and edit `src/pages/index.js`. Save your changes and the browser will update in real time!

## 🧐 What's inside?

A quick look at the top-level files and directories you'll see in a Gatsby project.

    .
    ├── node_modules
    ├── src
    ├── .gitignore
    ├── .prettierrc
    ├── gatsby-browser.js
    ├── gatsby-config.js
    ├── gatsby-node.js
    ├── gatsby-ssr.js
    ├── LICENSE
    ├── package-lock.json
    ├── package.json
    └── README.md

1.  **`/node_modules`**: This directory contains all of the modules of code that your project depends on (npm packages) are automatically installed.

2.  **`/src`**: This directory will contain all of the code related to what you will see on the front-end of your site (what you see in the browser) such as your site header or a page template. `src` is a convention for “source code”.

3.  **`.gitignore`**: This file tells git which files it should not track / not maintain a version history for.

4.  **`.prettierrc`**: This is a configuration file for [Prettier](https://prettier.io/). Prettier is a tool to help keep the formatting of your code consistent.

5.  **`gatsby-browser.js`**: This file is where Gatsby expects to find any usage of the [Gatsby browser APIs](https://www.gatsbyjs.org/docs/browser-apis/) (if any). These allow customization/extension of default Gatsby settings affecting the browser.

6.  **`gatsby-config.js`**: This is the main configuration file for a Gatsby site. This is where you can specify information about your site (metadata) like the site title and description, which Gatsby plugins you’d like to include, etc. (Check out the [config docs](https://www.gatsbyjs.org/docs/gatsby-config/) for more detail).

7.  **`gatsby-node.js`**: This file is where Gatsby expects to find any usage of the [Gatsby Node APIs](https://www.gatsbyjs.org/docs/node-apis/) (if any). These allow customization/extension of default Gatsby settings affecting pieces of the site build process.

8.  **`gatsby-ssr.js`**: This file is where Gatsby expects to find any usage of the [Gatsby server-side rendering APIs](https://www.gatsbyjs.org/docs/ssr-apis/) (if any). These allow customization of default Gatsby settings affecting server-side rendering.

9.  **`LICENSE`**: Gatsby is licensed under the MIT license.

10. **`package-lock.json`** (See `package.json` below, first). This is an automatically generated file based on the exact versions of your npm dependencies that were installed for your project. **(You won’t change this file directly).**

11. **`package.json`**: A manifest file for Node.js projects, which includes things like metadata (the project’s name, author, etc). This manifest is how npm knows which packages to install for your project.

12. **`README.md`**: A text file containing useful reference information about your project.

## 🎓 Learning Gatsby

Looking for more guidance? Full documentation for Gatsby lives [on the website](https://www.gatsbyjs.org/). Here are some places to start:

- **For most developers, we recommend starting with our [in-depth tutorial for creating a site with Gatsby](https://www.gatsbyjs.org/tutorial/).** It starts with zero assumptions about your level of ability and walks through every step of the process.

- **To dive straight into code samples, head [to our documentation](https://www.gatsbyjs.org/docs/).** In particular, check out the _Guides_, _API Reference_, and _Advanced Tutorials_ sections in the sidebar.

# 💫 Deploy
The below guidance is to get your website to deploy by your own RPi. You can either use **`gatsby serve`** ship with `gatsby-cli` for HTTP server, or, you can also use HTTPS server. In this tourtrial, we will use `Caddy` to serve HTTPS automatically.

## Get script start while RPi boot up
***Made the root have Node binary to where it was installed***
For example, if your `node-10.14.1` is instaled by `nvm`, then the path should be `$HOME/.nvm/versions/node/v10.14.1/bin/node`

  ```bash
  $ sudo ln -f /home/singu/.nvm/versions/node/v10.14.1/bin/node /usr/bin/node
  ```

## Create the service script

1. Let's make a `gatsby_server` service for daemon.

  ``` sh
  $ sudo nano /lib/systemd/system/gatsby_server.service
  ------->>> gatsby_server.service
  [Unit]
  Description=gatsby server
  After=multi-user.target

  [Service]
  Type=simple
  WorkingDirectory=/home/singu/my-gatsby-blog
  ExecStart=/home/singu/.nvm/versions/node/v10.14.1/bin/gatsby serve -H 0.0.0.0 -p 8080
  Restart=on-abort

  [Install]
  WantedBy=multi-user.target
  ```

  ```bash
  ctrl + x; Y; enter to save
  ```

2. Grab proper access right for the service file***

  ```bash
  $ sudo chmod 644 /lib/systemd/system/gatsby_server.service
  ```

3. Start the daemon service

  ```bash
  $ sudo systemctl daemon-reload

  # Enable service while boot up
  $ sudo systemctl enable gatsby_server.service

  # Start service
  $ sudo systemctl start gatsby_server.service
  ```

  ***Other Executions:***

  ```bash
  # Check status
  $ sudo systemctl status gatsby_server.service

  # Stop service
  $ sudo systemctl stop gatsby_server.service

  # Check service's log
  $ sudo journalctl -f -u gatsby_server.service

  # Disable service while boot up by
  $ sudo systemctl disable gatsby_server.service
  ```

By complete till here, you should be able to host an HTTP static website on your RPi.
However, Google Chrome is now officially treated with none HTTPS website as unsafe to visit. Meaning, HTTPS is getting more important for your website in the long run. Also, in some articles, leads HTTPS actually loads faster then HTTP/2 in the modern web browsers, so, for all the upside, let's move to use HTTPS by following steps.

[To find out more about HTTPS, here is a good start point.](https://doesmysiteneedhttps.com/)

## Setup HTTPS server

We will be use [Caddy](https://caddyserver.com/) to host our HTTPS Static website, it's an opensource project and automatic generate the TLS certificate for you. It's really plug and run solution.
<!-- 
1. Install Go lang for Caddy

***Choose the Go lang version for your computer***

  ```bash
  # To find your RPi model version
  $ sudo cat /proc/cpuinfo
  > ...
  > Hardware        : BCM2835
  > Revision        : a02082
  > Serial          : 00000000fa70701f
  > Model           : Raspberry Pi 3 Model B Rev 1.2

  # RPi 3 B use ARM64
  $ wget https://dl.google.com/go/go1.13.5.linux-arm64.tar.gz
  $ sudo tar -xvf go1.13.5.linux-arm64.tar.gz
  $ sudo mv go /usr/local/
  ```

2. Make PATH in *`~/.bashrc`* for Go

  ```bash
  $ nano ~/.bashrc
  >>> ~/.bashrc
  # Go lang root path
  export GOROOT=/usr/local/go
  export GOPATH=$HOME/https-server

  # Add GOBIN to the path
  export PATH=...:$GOROOT/bin:$GOPATH/bin:$PATH
  ```

  ```bash
  ctrl + x; and 'y' to save
  ```

Reload your *`~/.bashrc`* withou reboot or close and re-open terminal.

  ```bash
  $ source ~/.bashrc
  ```

3. Make a link for root us

  ```
  $ sudo ln -f $GOROOT/bin/go /usr/bin/go
  ```

4. Verify Go installation

  ```bash
  $ go version
  > go go1.13.5 linux/arm64
  ``` -->

## Download Caddy
  We will grab the binary and use it directly, quick and simple isn't it!
  
  ```bash
  $ wget https://github.com/caddyserver/caddy/releases/download/v1.0.4/caddy_v1.0.4_linux_arm64.tar.gz
  $ tar -xzf caddy_v1.0.4_linux_arm64.tar.gz caddy
  $ sudo mv ./caddy /usr/local/bin
  ```

### Verify Caddy installation

  ```bash
  move into our site
  $ cd $WEBSITE_PROJECT
  $ caddy
  > Activating privacy features... done.
  > Serving HTTP on port 2015
  > http://:2015
  ```

  ```bash
  ctrl + c; to stop
  ```

In this example, url is `www.diallelus.idv.tw`, but you should use yours. Gatsby puts the static file in *`$WEBSITE_PROJECT/public`*. Make sure you have **`gatsby build`** to generate the `public` content.

  1. Run `caddy` by command line tool

  ```bash
  $ sudo caddy -host www.diallelus.idv.tw -root ./public

  > The first time you run Caddy with a real hostname (not localhost), you'll be asked to enter your email address. 
  > This is because Caddy needs to verify you own the domain and to store the certificate safely on disk for you.

  # File descriptor limit 1024 is too low for production servers. At least 8192 is recommended
  $ ulimit -n 8192
  ```

  If everything looks ok, they we can make a `Caddyfile` for the project, then we don't need to type all the option in the future while start `Caddy`.

  2. create a customize `Caddyfile`

  ```bash
  $ cd $WEBSITE_PROJECT
  $ nano Caddyfile 
  >>> Caddyfile
  www.example.com
  root ./public
  ```

  ```bash
  ctrl + x; and 'y' to save
  ```

  Then you can simply run **`sudo caddy`** to start the server

### Caddy serve in daemon
Further more, we hope Caddy can serve our website in every boot up. So, again, let's make a daemon service.

1. Link Caddy binary to root

  ```bash
  $ sudo ln -f /usr/local/bin/caddy /usr/bin/caddy
  ```

2. make a service for Caddy to your website project

  
  ```bash
  $ sudo nano /lib/systemd/system/caddy_server.service
  ------->>> caddy_server.service
  [Unit]
  Description=caddy https server
  After=multi-user.target

  [Service]
  Type=simple
  WorkingDirectory=/home/singu/my-gatsby-blog
  ExecStart=/usr/local/bin/caddy
  Restart=on-abort

  [Install]
  WantedBy=multi-user.target
  ```

  3. Get previlage

    ```bash
    $ sudo chmod 644 /lib/systemd/system/caddy_server.service
    ```
  
  4. Reload-daemon, enable and start caddy service

  Before we start `caddy_server.service`, make sure caddy is not using the *same `Port`* of your `gatsby serve`, or, if you prefer to use same Port, then you should disable Gatsby serve before start caddy service.

  ```bash
  $ sudo systemctl stop gatsby_server.service
  $ sudo systemctl disable gatsby_server.service
  ```

  ```bash
  $ sudo systemctl daemon-reload
  $ sudo systemctl enable caddy_server.service
  $ sudo systemctl start caddy_server.service
  ```

Till here, you should be able to take a look of your website and see if the HTTPS is up and runnung!

## The tourtrial is now complete. Have fun and Happy coding!

### Below are Some other ENV setup for handy usage, take it if you like it. Cheers

## Bash ENV

  ```bash
  >>> ~/.bashrc
  ### Check and clean listening Port
  # check port
  function ckport { echo "Check all listened ports" | netstat -anp tcp | grep "$1"; }
  export -f ckport # export a function, for bash

  # kill PID who listening port (need to install lsof in advance)
  function klport { sudo lsof -i TCP:"$1" | grep LISTEN | awk '{print $2}' | sudo xargs kill -9 | echo "kill process was listening at port $1";}
  export -f klport # export a function, for bash

  ## Some handy alias
  ### renew gatsby website to master when using Gatsby serve
  alias renewgatsbyserve="git pull origin master &&\
  klport 8080 &&\
  gatsby build &&\
  sudo systemctl start gatsby_server.service"

  ### if using Caddy, then just simply git pull from pre-build static files.
  alias renewgatsby="git pull origin master"
  ```

### PATH management

  ```bash
  >>> ~/.bash
  # PATH
  # Projects
  export WEBSITE_PROJECT=$HOME/my-gatsby-blog

  # Node root path
  export NODEBIN=/usr/bin/node
  export NODEROOT=/home/singu/.nvm/versions/node/v10.14.1/bin/node

  # Go lang root path
  export GOBIN=/usr/local/go/bin
  export GOPATH=$HOME/https-server

  # ENV PATH
  export PATH=$NODEROOT:$NODEBIN:$GOROOT/bin:$GOPATH/bin:$PATH
  ```

## Clone project from GitLab and try out by gatsby develop 

  ```bash
  $ git clone git@gitlab.com:SingularityTim/my-gastby-blog-practice.git
  $ mv -f my-gastby-blog-practice my-gatsby-blog
  $ cd my-gatsby-blog
  $ gatsby build
  # Server gatsby we
  $ gatsby develop --host 0.0.0.0 --port 8080 --verbose
  ```