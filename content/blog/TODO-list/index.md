---
title: Let's start with some TODO list
date: "2019-12-18T02:16:37.121Z"
---

Well, to be able to have this site in the long run, I have initiated to listing some TODO items, this can help me track the actions and also see what I have completed. For sure it will keep growing in the coming future, and I will add a link to each item if I have a blog for it.

1. [x] ***[First, make a daemon service to handle the Gatsby serve. (Dec 19th)](/host-https-gatsby-blog/#4-caddy-serve-in-daemon)***

2. [x] ***[Second, set up HTTPS server. (Dec 25th)](/host-https-gatsby-blog/#-setup-https-server)***

3. [ ] Continuing - Add my side projects step by step and new posts.

        **---- Completed ----**

    a. [x] ***[How to use Raspberry Pi to host HTTPS website with Caddy and GatsbyJS](/host-https-gatsby-blog)***

    b. [x] ***[Config specific resolution LCD screen for PRi](/rpi-setup-7inch-lcd-screen)***

        **---- To Be Continued ----**

    c. [ ] ***[Make Reef Tank Spectrum Simulator by Use Raspberry Pi](/Spectrum-simulation-lighting-rpi)***

        **---- New Post Idea ----**

    d. [ ] ***[Auto Gardening System by User Raspberry Pi](#)***
    
4. [x] Hash navigation.

5. [ ] In blog search.

6. [x] Add google sitemap generator (Dec 28th).

7. [x] Implement SEO per blog, and image-photo for social medias (Dec 31st).

8. [ ] Add RSS feed.
