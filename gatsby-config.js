module.exports = {
  siteMetadata: {
    title: `Tim's TRYnERR Journey`,
    author: `Tim Lin`,
    keywords: ['Tim', 'Tim Lin', 'Journey', 'Blog'],
    description: `He loves cooking, keeping a reef tank, and proudly being a Maker who self-learned programming, 3D modeling, 3D printing, and electrical circuit design. Here is a playground, a library, but most importantly, a place to share with others about the projects that he has been working on and wish to learn more from the feedback. If you find the same interest as he does or think the post is helpful, please don't be hesitated to send him an email to share the excites.`,
    siteUrl: `https://www.timlin.tw`,
    nickname: `Tim`,
    email: `de.lintim@gmail.com`,
    social: {
      facebook: `damao.republic`,
      gitlab: {
        id: `SingularityTim`,
        project: {
          website: {
            name: `my-gastby-blog-practice`,
            latestBranch: 'master',
          },
        },
      },
    },
  },
  plugins: [
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-plugin-catch-links`,
      options: {
        excludePattern: /(excluded-link|external)/,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/blog`,
        name: `blog`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/assets`,
        name: `assets`,
      },
    },
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        extensions: [`.mdx`, `.md`],
        gatsbyRemarkPlugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 590,
            },
          },
          {
            resolve: `gatsby-remark-responsive-iframe`,
            options: {
              wrapperStyle: `margin-bottom: 1.0725rem`,
            },
          },
          `gatsby-remark-autolink-headers`,
          `gatsby-remark-prismjs`,
          `gatsby-remark-copy-linked-files`,
          `gatsby-remark-smartypants`,
        ],
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: `UA-154685820-1`,
      },
    },
    `gatsby-plugin-feed-mdx`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Tim's Blog`,
        short_name: `GatsbyJS`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `content/assets/gatsby-icon.png`,
      },
    },
    `gatsby-plugin-offline`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
  ],
}
