import React from "react"
import { StaticQuery, graphql } from "gatsby"

import Emoji from "../components/emoji"

function Footer() {
  return (
    <StaticQuery
      query={graphql`
        query {
          site {
            siteMetadata {
              nickname
              email
            }
          }
        }
      `}
      render={data => {
        const contact = data.site.siteMetadata
        return (
          <div id="footer">
            ©{new Date().getFullYear()}, Built by <Emoji symbol="&#x2665;" lable="heart" /> with
            {` `}
            <a href="https://www.gatsbyjs.org">Gatsby</a>
            {` and `}
            <a href="https://caddyserver.com/">Caddy</a>
            {`. `}
            <Emoji symbol="&#x1F4EE;" lable="postbox" />
            <a id='email' href={`mailto: ${contact.email}`}> Send {`${contact.nickname}`} an Email</a>
          </div>
        )
      }}
    />
  )
}

export default Footer