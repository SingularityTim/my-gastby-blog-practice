import React from "react"
import { Link, graphql } from "gatsby"
import Image from "gatsby-image"

import Bio from "../components/bio"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { rhythm } from "../utils/typography"

class BlogIndex extends React.Component {
  render() {
    const { data } = this.props
    const siteTitle = data.site.siteMetadata.title
    const siteKeywords = data.site.siteMetadata.keywords
    const siteCoverImage = data.coverImage.childImageSharp
    const siteDescription = data.site.siteMetadata.description
    const posts = data.allMdx.edges

    return (
      <Layout location={this.props.location} title={siteTitle}>
        <SEO
          title="Tim's TRYnERR Journey"
          keywords={siteKeywords}
          image={siteCoverImage.fluid}
        />
        <div id='description'>
          {siteDescription}
        </div>
        <br></br>
        <Bio />

        <Image
          fluid={siteCoverImage.fluid}
          alt=''
        />
        {posts.map(({ node }) => {
          const title = node.frontmatter.title || node.fields.slug
          return (
            <div key={node.fields.slug}>
              <h3
                style={{
                  marginBottom: rhythm(1 / 6),
                }}
              >
                <Link style={{ boxShadow: `none` }} to={node.fields.slug}>
                  {title}
                </Link>
              </h3>
              <small>{node.frontmatter.date}</small>
              <p
                dangerouslySetInnerHTML={{
                  __html: node.frontmatter.description || node.excerpt,
                }}
              />
            </div>
          )
        })}
      </Layout>
    )
  }
}

export default BlogIndex

export const pageQuery = graphql`
  query PageQuery {
    site {
      siteMetadata {
        title
        keywords
        description
      }
    }
    coverImage: file(absolutePath: { regex: "/cover-photo.jpeg/" }) {
      childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
        resize(width:1200) {
          src
          width
          height
        }
      }
    }
    allMdx(sort: { fields: [frontmatter___date], order: DESC }) {
      edges {
        node {
          excerpt
          fields {
            slug
          }
          frontmatter {
            date(formatString: "MMMM DD, YYYY")
            title
            description
          }
        }
      }
    }
  }
`
